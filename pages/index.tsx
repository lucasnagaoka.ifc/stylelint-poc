import { Container, Title } from "../src/styles/homepage";

export default function Home() {
  return (
    <Container>
      <Title>My page</Title>
    </Container>
  )
}
