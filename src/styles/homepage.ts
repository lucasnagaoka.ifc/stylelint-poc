import styled from 'styled-components'

export const Container = styled.main`
  width: 100%; /* replace it with "vw", for instance (https://stylelint.io/user-guide/rules/list/unit-allowed-list) */
  display: flex;
  justify-content: center;
`;

export const Title = styled.h1`
  color: green; /* replace it with invalid HEX, such as "#00" */
  font-size: 5rem; /* try to use a value in px instead (https://stylelint.io/user-guide/rules/list/unit-allowed-list) */
  text-align: center;
`;